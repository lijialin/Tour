package neu.tour;

import android.content.Intent;

import neu.tour.activity.MainActivity;

/**
 * Created by lijialin on 4/15/16.
 */
public class MainActivityTest extends android.test.ActivityUnitTestCase<MainActivity> {

    private MainActivity activity;

    public MainActivityTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        Intent intent = new Intent(getInstrumentation().getTargetContext(),
                MainActivity.class);
        this.startActivity(intent, null, null);
        activity = getActivity();
    }

}
