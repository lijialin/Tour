package neu.tour.activity;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;

import neu.tour.R;

/**
 * Created by lijialin on 4/29/16.
 */
public class MeFragement extends Fragment {

    private String path = "/AATour/edit"; //存储拍摄照片的路径
    private File[] files;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_me,container,false);
        // 绑定Layout里的ListView
//        ListView list = (ListView) view.findViewById(R.id.listView);
        final GridView gv = (GridView) view.findViewById(R.id.photo_wall);

        File pathFile = new File(Environment.getExternalStorageDirectory() + path);
        files = pathFile.listFiles();

        // if files is empty
        if(files == null){
            TextView tipTextView = (TextView) view.findViewById(R.id.nothing_tip);
            tipTextView.setVisibility(View.VISIBLE);
        } else {
            ImageAdapter ia = new ImageAdapter(this.getActivity(), files);
            gv.setAdapter(ia);//为GridView设置数据适配器
        }

        gv.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(files[(int)id]), "image/*");
                        startActivityForResult(intent, 1);
                    }
                }
        );

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK){
            Uri uri = data.getData();
            System.out.println(uri);
//            Cursor cursor = this.getContentResolver().query(uri, null, null, null, null);
//            cursor.moveToFirst();
//            for (int i = 0; i < cursor.getColumnCount(); i++){// 取得图片uri的列名和此列的详细信息
//                System.out.println(i + "-" + cursor.getColumnName(i) + "-" + cursor.getString(i));
//            }
        }
    }

    public class ImageAdapter extends BaseAdapter {
        // 定义Context
        private Context mContext;
        // 定义图片文件列表 即图片源
        private File[]   mFiles;

        public ImageAdapter(Context c, File[] files) {
            mContext = c;
            mFiles = files;
        }

        // 获取图片的个数
        public int getCount()
        {
            if (mFiles != null)
                return mFiles.length;
            else
                return 0;
        }

        // 获取图片在库中的位置
        public Object getItem(int position)
        {
            return position;
        }


        // 获取图片ID
        public long getItemId(int position)
        {
            return position;
        }



        public View getView(int position, View convertView, ViewGroup parent)
        {
            final ImageView imageView;
            if (convertView == null)
            {
                // 给ImageView设置资源
                imageView = new ImageView(mContext);
                // 设置布局 图片120×120显示
                imageView.setLayoutParams(new GridView.LayoutParams(300, 300));
                // 设置显示比例类型
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            } else {
                imageView = (ImageView) convertView;
            }

            imageView.setImageURI(Uri.fromFile(mFiles[position]));

//            imageView.setImageResource(mImageIds[position]);
            return imageView;
        }

    }

}
