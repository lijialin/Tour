package neu.tour.activity;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;

import neu.tour.R;
import neu.tour.support.HttpConnectionUtil;
import neu.tour.support.SaveDataToFile;

/**
 * Created by lijialin on 4/25/16.
 */
public class ListFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragemtnt_list,container,false);
        // 绑定Layout里的ListView
        final ListView list = (ListView) view.findViewById(R.id.listView);

        // get json data from SDCard
        SaveDataToFile s = new SaveDataToFile();
        try {
            String result = s.getJsonDataFromSDCard("/AATour/data");

            final ArrayList<HashMap<String, Object>> listItem
                    = HttpConnectionUtil.decodeJsonToPoint(result);

            // 生成一个SimpleAdapter类型的变量来填充数据
            final SimpleAdapter listItemAdapter = new SimpleAdapter(
                    getActivity(),
                    //view.getContext(),// this是当前Activity的对象
                    listItem,// 数据源 为填充数据后的ArrayList类型的对象
                    R.layout.fragement_list_item,// 子项的布局.xml文件名
                    new String[]{"name"},
                    //这个String数组中的元素就是list对象中的列，list中有几这个数组中就要写几列。
                    new int[]{R.id.name}
            );//值是对应XML布局文件中的一个ImageView,三个TextView的id


            // 添加并显示
            list.setAdapter(listItemAdapter);

            // add click listener
            list.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    HashMap<String, Object> item = listItem.get(position);
                    int point_id = (int) item.get("id");
                    String name = (String) item.get("name");
                    String longitude = (String) item.get("longitude");
                    String latitude = (String) item.get("latitude");
                    String cross_pictures = (String) item.get("cross_pictures");
                    Intent intent = new Intent();
                    intent.setClass(getActivity(), PointActivity.class);
                    intent.putExtra("point_id", point_id);
                    intent.putExtra("name", name);
                    intent.putExtra("longitude", longitude);
                    intent.putExtra("latitude", latitude);
                    intent.putExtra("cross_pictures", cross_pictures);
                    getActivity().startActivity(intent);
                }
            });

            // set long clicklistener
            list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                    HashMap<String, Object> item = listItem.get(position);
                    final int item_id = (int) item.get("id");
                    final int p = position;
                    new AlertDialog.Builder(getActivity()).setTitle("系统提示")//设置对话框标题
                            .setMessage("Delete this point?")//设置显示的内容
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {//添加确定按钮
                                @Override
                                public void onClick(DialogInterface dialog, int which) {//确定按钮的响应事件
                                    //new thread to delete
                                    new Thread(){
                                        public void run(){
                                            try{
//                                                String server_url = "http://120.27.118.23:5678/cross/place/" + item_id +"/";
//                                                HttpConnectionUtil.doDelete(server_url);
                                                HttpConnectionUtil.doDelete(HttpConnectionUtil.PLACEURL+item_id +"/");
                                                if(HttpConnectionUtil.result == "success"){
                                                    listItem.remove(p);
                                                    listItemAdapter.notifyDataSetChanged();
                                                }
                                            }
                                            catch(Exception e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }.start();

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {//添加返回按钮
                                @Override
                                public void onClick(DialogInterface dialog, int which) {//响应事件
                                    // TODO Auto-generated method stub
                                    dialog.dismiss();
                                }
                            }).show();//在按键响应事件中显示此对话框
                    return true;
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        Button addButton = (Button) view.findViewById(R.id.addPointbutton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), AddPointActivity.class);
                getActivity().startActivity(intent);
            }
        });

        return view;
    }



}
