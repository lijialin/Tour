package neu.tour.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import neu.tour.R;

public class AddPictureActivity extends AppCompatActivity {

    private int point_id;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_picture);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();//从上一个Activity获取参数图片
        point_id = bundle.getInt("point_id");
        name = bundle.getString("name");
        //TODO add a new picture to server

    }
}
