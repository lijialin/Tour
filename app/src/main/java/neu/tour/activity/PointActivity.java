package neu.tour.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import neu.tour.R;
import neu.tour.support.HttpConnectionUtil;
import neu.tour.support.Myadapter;

public class PointActivity extends Activity {

    private Myadapter myadapter;
    private int point_id; //point id
    private String cross_pictures;
    private String name;
    private String longitude;
    private String latitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point);

        ListView list = (ListView) findViewById(R.id.piclistView);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();//从上一个Activity获取参数图片
        point_id = bundle.getInt("id");
        cross_pictures = bundle.getString("cross_pictures");
        name = bundle.getString("name");
        longitude = bundle.getString("longitude");
        latitude = bundle.getString("latitude");

        TextView nameTextView = (TextView) findViewById(R.id.point_name);
        nameTextView.setText(name);
        TextView positionTextView = (TextView) findViewById(R.id.point_position);
        positionTextView.setText(longitude+","+latitude);

        final ArrayList<HashMap<String, Object>> listItem;
        try {
            listItem = HttpConnectionUtil.decodeCrossPicturesJsonToPoint(cross_pictures);

            List<String> URL = new ArrayList<String>();
            List<String> title = new ArrayList<String>();
            for(int i=0;i<listItem.size();i++){
                //URL.add((String)listItem.get(i).get("picture"));
                title.add((String)listItem.get(i).get("title"));
            }
            myadapter = new Myadapter(PointActivity.this, URL, title, list);

            // 添加并显示
            list.setAdapter(myadapter);

            // add click listener
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    HashMap<String, Object> item = listItem.get(position);
                    String title = (String) item.get("title");
                    String longitude = (String) item.get("longitude");
                    String latitude = (String) item.get("latitude");
                    Intent intent = new Intent();
                    intent.setClass(PointActivity.this, PictureActivity.class);
                    intent.putExtra("title", title);
                    intent.putExtra("longitude", longitude);
                    intent.putExtra("latitude", latitude);
                    PointActivity.this.startActivity(intent);
                }
            });

            // set long clicklistener
            list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    new AlertDialog.Builder(PointActivity.this).setTitle("系统提示")//设置对话框标题
                            .setMessage("Delete this point?")//设置显示的内容
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {//添加确定按钮
                                @Override
                                public void onClick(DialogInterface dialog, int which) {//确定按钮的响应事件

                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {//添加返回按钮
                                @Override
                                public void onClick(DialogInterface dialog, int which) {//响应事件
                                    dialog.dismiss();
                                }
                            }).show();//在按键响应事件中显示此对话框
                    return true;
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Button addButton = (Button) findViewById(R.id.addPicturebutton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(PointActivity.this, AddPictureActivity.class);
                intent.putExtra("point_id", point_id);
                intent.putExtra("name", name);
                PointActivity.this.startActivity(intent);
            }
        });

    }


}
