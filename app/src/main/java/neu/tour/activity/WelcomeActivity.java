package neu.tour.activity;

/**
 * 首页Welcome界面
 */
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;


import java.util.Timer;
import java.util.TimerTask;

import neu.tour.R;
import neu.tour.support.InitSystemBar;


public class WelcomeActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        InitSystemBar.initSystemBar(this);
        setContentView(R.layout.activity_welcome);

        //计时1s
        Timer timer = new Timer();
        TimerTask task = new TimerTask(){
            @Override
            public void run(){
                jumpActivity();
            }
        };
        timer.schedule(task, 1000);
    }

    /**
     * 跳转到MainActivity
     */
    private void jumpActivity() {
        Intent intent = new Intent();
//        intent.setClass(WelcomeActivity.this, MainActivity.class);
        intent.setClass(WelcomeActivity.this, NavigateActivity.class);
        WelcomeActivity.this.startActivity(intent);
        finish();
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

}