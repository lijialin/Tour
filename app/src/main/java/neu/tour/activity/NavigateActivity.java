package neu.tour.activity;

import android.app.Activity;
//import android.app.Fragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import neu.tour.R;
import neu.tour.support.HttpConnectionUtil;
import neu.tour.support.SaveDataToFile;


public class NavigateActivity extends Activity
        implements RadioGroup.OnCheckedChangeListener{

    private RadioGroup rg_tab_bar;
    private RadioButton rb_list;

    private ListFragment fg1;
    private MapFragment fg2;
    private MeFragement fg3;
    private FragmentManager fManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigate);
        fManager = getFragmentManager();

        //new thread to download server data
        downloadData();

        rg_tab_bar = (RadioGroup) findViewById(R.id.rg_tab_bar);
        rg_tab_bar.setOnCheckedChangeListener(this);
        //获取第一个单选按钮，并设置其为选中状态
        rb_list = (RadioButton) findViewById(R.id.rb_list);
        rb_list.setChecked(true);
    }


    public void downloadData(){
        //new thread to download server data
        new Thread(){
            public void run(){
                try{
//                    String server_url = "http://120.27.118.23:5678/cross/place/";
                    Map<String, Object> map = new HashMap<String, Object>();
                    HttpConnectionUtil.doGet(HttpConnectionUtil.PLACEURL, map);
                    String result = HttpConnectionUtil.result;
//                    System.out.println("asdf "+result);
                    //save result to SD file
                    String savePath = "/AATour/data"; //存储拍摄照片的路径
                    SaveDataToFile s = new SaveDataToFile(savePath, 1);
                    s.saveJsonToSDCard(result);
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        FragmentTransaction fTransaction = fManager.beginTransaction();
        hideAllFragment(fTransaction);
        switch (checkedId){
            case R.id.rb_list:
                if(fg1 == null){
                    fg1 = new ListFragment();
                    fTransaction.add(R.id.ly_content,fg1);
                }else{
                    fTransaction.show(fg1);
                }
                break;
            case R.id.rb_map:
                if(fg2 == null){
                    fg2 = new MapFragment();
                    fTransaction.add(R.id.ly_content,fg2);
                }else{
                    fTransaction.show(fg2);
                }
                break;
            case R.id.rb_me:
                if(fg3 == null){
                    fg3 = new MeFragement();
                    fTransaction.add(R.id.ly_content,fg3);
                }else{
                    fTransaction.show(fg3);
                }
                break;
        }
        fTransaction.commit();
    }

    //隐藏所有Fragment
    private void hideAllFragment(FragmentTransaction fragmentTransaction){
        if(fg1 != null)fragmentTransaction.hide(fg1);
        if(fg2 != null)fragmentTransaction.hide(fg2);
        if(fg3 != null)fragmentTransaction.hide(fg3);
    }



}