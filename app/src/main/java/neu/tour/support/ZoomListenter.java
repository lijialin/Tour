package neu.tour.support;

/**
 * Created by lijialin on 16-3-9.
 */
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class ZoomListenter implements OnTouchListener {

    private int mode = 0;
    private float oldDist;

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN://在第一个点被按下时触发
                mode = 1;
                // 调用触摸对焦函数
                autoFocus(event);
                break;
            case MotionEvent.ACTION_UP://当屏幕上唯一的点被放开时触发
                mode = 0;
                break;
            //当屏幕上已经有一个点被按住，此时再按下其他点时触发
            case MotionEvent.ACTION_POINTER_UP:
                mode -= 1;
                break;
            //当屏幕上有多个点被按住，松开其中一个点时触发
            case MotionEvent.ACTION_POINTER_DOWN:
                oldDist = spacing(event);
                mode += 1;
                break;

            case MotionEvent.ACTION_MOVE://当有点在屏幕上移动时触发
                if (mode >= 2) {
                    float newDist = spacing(event);
                    if (newDist > oldDist + 1) {
                        zoom(newDist - oldDist);
                        oldDist = newDist;
                    }
                    if (newDist < oldDist - 1) {
                        zoom(newDist - oldDist);
                        oldDist = newDist;
                    }
                }
                break;
        }
        return true;
    }

    public void zoom(float f) {
    }

    public void autoFocus(MotionEvent event){
    }

    private float spacing(MotionEvent event) {
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float)Math.sqrt(x * x + y * y);
    }

}
