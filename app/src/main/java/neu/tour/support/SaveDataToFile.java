package neu.tour.support;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Environment;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lijialin on 16-3-7.
 */
public class SaveDataToFile {

    private Date date; //时间
    private String filename; //文件名
    private String path; //路径

    /**
     * 获取文件名
     */
    public String getFilename() {
        return this.filename;
    }

    /**
     * 构造方法
     */
    public SaveDataToFile(){
    }

    public SaveDataToFile(String path, int type) {
        // type 0:picture  1:txt
        if(type == 0) {
            this.date = new Date();
            SimpleDateFormat format =
                    new SimpleDateFormat("yyyyMMddHHmmss");
            this.filename = format.format(date) + ".jpg";
        }else if(type == 1){
            this.filename = "json.txt";
        }
        this.path = path;
    }

    public SaveDataToFile(String path, String filename) {
        this.date = new Date();
        this.path = path;
    }


    /**
     * 存储picture至SD文件中
     */
    public void saveToSDCard(Bitmap b) throws IOException {
        File fileFolder = new File(
                Environment.getExternalStorageDirectory() + path);
        // 如果目录不存在，则创建一个目录
        if (!fileFolder.exists()) {
            fileFolder.mkdirs();//创建多级目录，要用mkdirs
        }
        File jpgFile = new File(fileFolder, filename);
        // 文件输出流
        FileOutputStream outputStream = new FileOutputStream(jpgFile);
        b.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
        outputStream.flush();
        outputStream.close(); // 关闭输出流
    }


    /**
     * 存储Json至SD文件中
     */
    public void saveJsonToSDCard(String json) throws IOException {
        File fileFolder = new File(
                Environment.getExternalStorageDirectory() + path);
        // 如果目录不存在，则创建一个目录
        if (!fileFolder.exists()) {
            fileFolder.mkdirs();//创建多级目录，要用mkdirs
        }
        File txtFile = new File(fileFolder, filename);
        FileOutputStream outputStream = new FileOutputStream(txtFile);
        outputStream.write(json.getBytes());
        outputStream.flush();
        outputStream.close();
    }


    /**
     * 删除文件
     */
    public void deleteFile(File file) {
        if (file.exists()) { // 判断文件是否存在
            if (file.isFile()) { // 判断是否是文件
                file.delete();
            } else if (file.isDirectory()) { // 否则如果它是一个目录
                File files[] = file.listFiles(); // 声明目录下所有的文件 files[];
                for (int i = 0; i < files.length; i++) { // 遍历目录下所有的文件
                    this.deleteFile(files[i]); // 把每个文件 用这个方法进行迭代
                }
            }
            file.delete();
        }
    }

    public String getJsonDataFromSDCard(String path) throws IOException{
        File fileFolder = new File(
                Environment.getExternalStorageDirectory() + path);
        String filename = "json.txt";
        File txtFile = new File(fileFolder, filename);
        FileInputStream inputStream = new FileInputStream(txtFile);
        byte[] bytes = new byte[1024];
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        while (inputStream.read(bytes) != -1) {
            arrayOutputStream.write(bytes, 0, bytes.length);
        }
        inputStream.close();
        arrayOutputStream.close();
        String content = new String(arrayOutputStream.toByteArray());
        return content;
    }
}