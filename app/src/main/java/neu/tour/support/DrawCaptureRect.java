package neu.tour.support;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.view.View;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by lijialin on 16-3-9.
 */
class DrawCaptureRect extends View {

    private int mcolorfill;
    private int mleft, mtop, mwidth, mheight;
    public DrawCaptureRect(Context context, int left, int top, int width, int height, int colorfill) {
        super(context);
        this.mcolorfill = colorfill;
        this.mleft = left;
        this.mtop = top;
        this.mwidth = width;
        this.mheight = height;
    }

    @Override
    protected void onDraw(final Canvas canvas) {
        Paint mpaint = new Paint();
        mpaint.setColor(mcolorfill);
        mpaint.setStyle(Paint.Style.FILL);
        mpaint.setStrokeWidth(10.0f);
        canvas.drawLine(mleft, mtop, mleft+mwidth/4, mtop, mpaint);
        canvas.drawLine(mleft+mwidth/4*3, mtop, mleft+mwidth, mtop, mpaint);
        canvas.drawLine(mleft+mwidth, mtop, mleft+mwidth, mtop+mheight/4, mpaint);
        canvas.drawLine(mleft+mwidth, mtop+mheight/4*3, mleft+mwidth, mtop+mheight, mpaint);
        canvas.drawLine(mleft, mtop, mleft, mtop+mheight/4, mpaint);
        canvas.drawLine(mleft, mtop+mheight/4*3, mleft, mtop+mheight, mpaint);
        canvas.drawLine(mleft, mtop+mheight, mleft+mwidth/4, mtop+mheight, mpaint);
        canvas.drawLine(mleft+mwidth/4*3, mtop+mheight, mleft+mwidth, mtop+mheight, mpaint);
        super.onDraw(canvas);
    }
}
