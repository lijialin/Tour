package neu.tour.support;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;


/**
 * Created by lijialin on 16-3-7.
 */
public class HandlePic {

    private Mat src = new Mat();
    private Mat dst = new Mat();

    public HandlePic() {

    }

    /**
     * 改变图片大小
     */
    public Bitmap changeBitmapSize(Bitmap b, float w, float h) {
        //pw 宽比
        float pw = w/(float)b.getWidth();
        //ph 高比
        float ph = h/(float)b.getHeight();
        //长和宽放大缩小的比例
        Matrix matrix = new Matrix();
        matrix.postScale(pw, ph);
        // after是缩放后的图片
        Bitmap after = Bitmap.createBitmap(b, 0, 0, b.getWidth(),
                b.getHeight(), matrix, true);
        return after;
    }


    /**
     * 自动生成边缘图
     */
    public Bitmap getBorder(Bitmap srcb) {
        // bitmap 转 Mat; src 是4通道图
        Utils.bitmapToMat(srcb, src);//初始化源矩阵
        // 边缘检测 Canny算子; dst是1通道图
        Imgproc.Canny(src, dst, 100, 150);
        // 旋转90度
//        transpose(dst,dst);
//        flip(dst,dst,1);
        // 提取透明图片android 双指缩放图片
        int width = dst.width();
        int height = dst.height();
        Bitmap b = Bitmap.createBitmap(
                width, height, Bitmap.Config.ARGB_4444);
        //Mat 转 bitmap
        Utils.matToBitmap(dst, b);
        //循环每个像素点
        for(int i=0; i<b.getWidth(); i++) {
            for(int j=0; j<b.getHeight(); j++) {
                if(b.getPixel(i,j) != Color.BLACK ){
                    b.setPixel(i,j,Color.GREEN);
                }else {
                    b.setPixel(i,j,Color.argb(0,0,0,0));
                }
            }
        }
        return b;
    }
}
