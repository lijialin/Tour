package neu.tour.support;

/**
 * Created by lijialin on 2/10/16.
 */

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.net.HttpURLConnection;

import neu.tour.R;

public class Poi implements Serializable {

    private static final long serialVersionUID = -758459502806858414L;//验证版本一致性
    private int id;//id
    private double lat; // 精度
    private double lng; //纬度
    private int[] imgList;//图片列表
    private int[] imgYear;//图片年代
    private int imgId;//图片ID
    private String name;//名称
    private String distance;//距离
    private String title; // 标题
    private String path; // 标题的路径
    private int love;//赞数量

    public static List<Poi> poi = new ArrayList<>();
    public static boolean isGetDataFromServer;

    static{
        isGetDataFromServer = false;
//        isGetDataFromServer = true;
        if(isGetDataFromServer) {
            new Thread() {
                public void run() {
                    getDataFromServer();
                };
            }.start();
        }else{
            getDataFromLocal();
        }
    }


    public static String getDataFromServer(){
//        System.out.println("connect server");
//        String server_url = "http://120.27.118.23:5678/cross/place/";
        String server_url = "http://www.baidu.com";
        try {
            URL url = new URL(server_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(6000);
            conn.setRequestMethod("GET");
//            conn.setDoInput(true);
            int code = conn.getResponseCode();
            if (code != 200) {
                System.out.println("request failed");
                throw new RuntimeException("request failed");
            } else {
                InputStream input_stream = conn.getInputStream();
//                BufferedInputStream buffer = new BufferedInputStream(input_stream);
                return changeInputStream(input_stream);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    public static String changeInputStream(InputStream input_stream){
        ByteArrayOutputStream output_stream = new ByteArrayOutputStream();
        byte[] data = new byte[1024];
        int len = 0;
        String result = "";
        if(input_stream != null){
            try{
                while((len = input_stream.read(data)) != -1 ){
                    output_stream.write(data, 0, len);
                }
                result = new String(output_stream.toByteArray(), "UTF-8");
//                System.out.println("asdf"+result);
//                int len = result.length();
                try{
                    JSONObject jsonObject = new JSONObject(result);
                    int jsonlen = jsonObject.length();
                    for(int i = 0; i< jsonlen; i++){
                        int id = jsonObject.getInt("id");
                        double lng = jsonObject.getDouble("longitude");
                        double lat = jsonObject.getDouble("latitude");
                        JSONArray cross_pictures = jsonObject.getJSONArray("cross_pictures");
                        String name = jsonObject.getString("name");

                        int piclen = cross_pictures.length();
                        int[] piclist = {};
//                        for(int j = 0; j < piclen; j++){
//                            int pic_id = cross_pictures.getInt("id");
//                            piclist[j] = cross_pictures.getString("detail_url");
//                        }
//                        poi.add(new Poi(1, 41.774338,123.426406,
//                                new int[] {R.drawable.i1_1900, R.drawable.i1_1910,
//                                        R.drawable.i1_1922, R.drawable.i1_1933,
//                                        R.drawable.i1_1944, R.drawable.i1_1970,
//                                        R.drawable.i1_1980},
//                                new int[] {1900, 1910, 1922, 1933, 1944, 1970, 1980},
//                                "东北大学", "此处放title",
//                                "http://blog.csdn.net/u010429424", "", 12));
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }

            }catch(IOException e){
                e.printStackTrace();
            }
        }
//        System.out.println(input_stream);
        return result;
    }



    public static void getDataFromLocal(){
        poi.add(new Poi(1, 41.774338,123.426406,
                new int[] {R.drawable.i1_1900, R.drawable.i1_1910,
                        R.drawable.i1_1922, R.drawable.i1_1933,
                        R.drawable.i1_1944, R.drawable.i1_1970,
                        R.drawable.i1_1980},
                new int[] {1900, 1910, 1922, 1933, 1944, 1970, 1980},
                "东北大学", "此处放title",
                "http://blog.csdn.net/u010429424", "", 12));
        poi.add(new Poi(3,41.805405,123.464656,
                new int[] {R.drawable.i3_1967, R.drawable.i3_1978,
                        R.drawable.i3_1989, R.drawable.i3_1990},
                new int[] {1967, 1978, 1989, 1990},
                "中街", "此处放title",
                "http://www.hao123.com", "", 456));
    }

    public Poi(int id,double lat, double lng,
               int[] imgList, int[] imgYear, String name,
               String title, String path,String distance, int love){
        super();
        this.id = id;
        this.lat = lat;
        this.lng = lng;
        this.imgList = imgList;
        this.imgYear = imgYear;
        this.name = name;
        this.title = title;
        this.path = path;
        this.distance = distance;
        this.love = love;
    }

    /**
     * id
     */
    public double getId(){
        return id;
    }

    /**
     * imgId
     */
    public int getImgId(){
        return imgId;
    }

    public void setImgId(int imgId){
        this.imgId = imgId;
    }

    /**
     * lat
     */
    public double getLat(){
        return lat;
    }

    public void setLat(double lat){
        this.lat = lat;
    }

    /**
     * lng
     */
    public double getLng(){
        return lng;
    }

    public void setLng(double lng){
        this.lng = lng;
    }

    /**
     * name
     */
    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    /**
     * title
     */
    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    /**
     * path
     */
    public String getPath(){
        return path;
    }

    public void setPath(String title){
        this.path = path;
    }

    /**
     * imgList
     */
    public int[] getImgList(){
        return imgList;
    }

    public void setImgList(int[] imgList){
        this.imgList = imgList;
    }

    /**
     * imgYear
     */
    public int[] getImgYear(){
        return imgYear;
    }

    public void setImgYear(int[] imgYear){
        this.imgYear = imgYear;
    }

    /**
     * distance
     */
    public String getDistance(){
        return distance;
    }

    public void setDistance(String distance){
        this.distance = distance;
    }

    /**
     * love
     */
    public int getLove(){
        return love;
    }

    public void setLove(int love){
        this.love = love;
    }

}
